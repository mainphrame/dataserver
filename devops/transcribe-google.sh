#!/bin/sh
rm -rf /devops/python-docs-samples/speech/api-client/env
rm /devops/scratch/onsite.flac*
wget http://pots.robotagrex.com/onsite.flac -O /devops/scratch/onsite.flac
export GOOGLE_APPLICATION_CREDENTIALS=/devops/officer-rainbow-key.json
env
#git clone https://github.com/GoogleCloudPlatform/python-docs-samples.git
cd /devops/python-docs-samples/speech/api-client
/usr/bin/virtualenv --python=/usr/bin/python2 env
echo "just ran virtualenv env"
. /devops/python-docs-samples/speech/api-client/env/bin/activate
pip install -r /devops/python-docs-samples/speech/api-client/requirements.txt
cd /devops/python-docs-samples/speech/api-client
/devops/python-docs-samples/speech/api-client/env/bin/python ./transcribe.py /devops/scratch/onsite.flac > /options/officer-rainbow/onsite-google-raw.txt
deactivate
cat /options/officer-rainbow/onsite-google-raw.txt | jq -r '.[]' | grep transcript |  tr '[:upper:]' '[:lower:]' | sed -e 's/"transcript"://g' | awk '{$1=$1};1' | tr -d "\"" > /options/officer-rainbow/onsite-google-clean.txt
