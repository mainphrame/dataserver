#!/bin/bash

TODAYS_DATE=`date +'%B %d, %Y'`
SITE_DATE=`date +'%m/%d/%Y'`
YEAR=`date +'%Y'`
DROP_MSG_FILE=jams-dropmessage.txt
RUN_DIR=/options/officer-rainbow #this directory better not exist!
GET_URL=http://www.jamstesting.com #no slash at the end please
GET_URI=jams.html
GET_URI_REAL="jamsTestDates"
GET_HOST=`echo $GET_URL | sed 's/http:\/\///g' | sed 's/https:\/\///g'`
QUALIFIER_1=248.454.0601

echo "Site Date = $SITE_DATE"
sleep 1
echo "System Date = $TODAYS_DATE"
echo $DROP_MSG_FILE
sleep 1
echo $RUN_DIR
sleep 1
echo $GET_URL
sleep 1
echo $GET_URI
sleep 1
echo $QUALIFIER_1

mkdir -p $RUN_DIR

/bin/rm -rf $RUN_DIR/$GET_URI*
/bin/rm -rf $RUN_DIR/$DROP_MSG_FILE
/bin/touch $RUN_DIR/$DROP_MSG_FILE


echo "This file is named $RUN_DIR/$DROP_MSG_FILE" >> $RUN_DIR/$DROP_MSG_FILE

if [ `nc -w5 -z $GET_HOST 80 ; echo $?` = 1 ]; then
        echo "The site appears to be OFFLINE" >&2
        echo "The site appears to be OFFLINE - the http connection failed" >> $RUN_DIR/$DROP_MSG_FILE
                sleep 5
                exit 1
        else echo "The site appears to be ONLINE"
                echo "The site appears to be ONLINE - the http connection succeeded" >> $RUN_DIR/$DROP_MSG_FILE
                sleep 5
fi

/usr/bin/wget -O $RUN_DIR/$GET_URI $GET_URL/$GET_URI_REAL
/bin/echo "Last run date is `date`" >>  $RUN_DIR/$DROP_MSG_FILE

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep $QUALIFIER_1 ; then
   /bin/echo "Website appears online - conducting secondary test." >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "The website is possibly down. Call $QUALIFIER_1." >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep "$SITE_DATE" ; then
   /bin/echo "Website appears online - today's date matches." >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "The website is possibly down. Call $QUALIFIER_1." >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Yellow ; then
   /bin/echo "Yellow WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Yellow was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Blue ; then
   /bin/echo "Blue WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Blue was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Purple ; then
   /bin/echo "Purple WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Purple was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Green ; then
   /bin/echo "Green WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Green was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Fuschia ; then
   /bin/echo "Fuschia WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Fuschia was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Gray ; then
   /bin/echo "Gray WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Gray was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Beige ; then
   /bin/echo "Beige WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Beige was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Ivory ; then
   /bin/echo "Ivory WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Ivory was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Magenta ; then
   /bin/echo "Magenta WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Magenta was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Peach ; then
   /bin/echo "Peach WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Peach was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Red ; then
   /bin/echo "Red WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Red was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Brown ; then
   /bin/echo "Brown WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Brown was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Gold ; then
   /bin/echo "Gold WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Gold was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Lavender ; then
   /bin/echo "Lavender WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Lavender was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Teal ; then
   /bin/echo "Teal WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Teal was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Amber ; then
   /bin/echo "Amber WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Amber was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Pink ; then
   /bin/echo "Pink WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Pink was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep White ; then
   /bin/echo "White WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "White was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Coral ; then
   /bin/echo "Coral WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Coral was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Orange ; then
   /bin/echo "Orange WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Orange was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Black ; then
   /bin/echo "Black WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Black was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Burgundy ; then
   /bin/echo "Burgundy WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Burgundy was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Lilac ; then
   /bin/echo "Lilac WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Lilac was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Peach ; then
   /bin/echo "Peach WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Peach was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Plum ; then
   /bin/echo "Plum WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Plum was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

wget -qO- $GET_URL/$GET_URI_REAL | sed -n "/<td>/,/<\/td>/p" | grep -v "</tr>" | sed 's/<td>//g' | sed 's/<\/td>//g' | sed "s@$SITE_DATE@@g" | sed -n '/<\/table>/q;p' | sed 's/...//' >> $RUN_DIR/$DROP_MSG_FILE

/bin/cat $RUN_DIR/$DROP_MSG_FILE

