#!/bin/bash

TODAYS_DATE=$(date +'%m/%d/%Y')
YEAR=`date +'%Y'`
DROP_MSG_FILE=onsite-dropmessage.txt
RUN_DIR=/options/officer-rainbow #this directory better not exist!
GET_URL=http://onsitesubstanceabusetesting.com #no slash at the end please
GET_URI_REAL=Daily-Testing-Colors.html
GET_URI=onsite.html
SITE_DATE=`echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.innerText14_ContentDiv"| lynx -stdin -dump | grep $YEAR`
GET_HOST=`echo $GET_URL | sed 's/http:\/\///g'`
QUALIFIER_1=248.242.0798

echo "Site Date = $SITE_DATE"
sleep 1
echo "System Date = $TODAYS_DATE"
echo $DROP_MSG_FILE
sleep 1
echo $RUN_DIR
sleep 1
echo $GET_URL
sleep 1
echo $GET_URI
sleep 1
echo $QUALIFIER_1
sleep 1
echo $GET_HOST

mkdir -p $RUN_DIR

/bin/rm -rf $RUN_DIR/$GET_URI*
/bin/rm -rf $RUN_DIR/$DROP_MSG_FILE
/bin/touch $RUN_DIR/$DROP_MSG_FILE

echo $TODAYS_DATE
sleep 1

echo "This file is named $RUN_DIR/$DROP_MSG_FILE" >> $RUN_DIR/$DROP_MSG_FILE

if [ `nc -w5 -z $GET_HOST 80 ; echo $?` = 1 ]; then
    	echo "The site appears to be OFFLINE" >&2
	echo "The site appears to be OFFLINE - the http connection failed" >> $RUN_DIR/$DROP_MSG_FILE
		sleep 5
		exit 1
	else echo "The site appears to be ONLINE"
		echo "The site appears to be ONLINE - the http connection succeeded" >> $RUN_DIR/$DROP_MSG_FILE
		sleep 5
fi

/usr/bin/wget -O $RUN_DIR/$GET_URI $GET_URL/$GET_URI_REAL
/bin/echo "Last run date is `date`" >>  $RUN_DIR/$DROP_MSG_FILE

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep $QUALIFIER_1 ; then
   /bin/echo "Website appears online - conducting secondary test." >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "The website is possibly down. Call $QUALIFIER_1." >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep $TODAYS_DATE ; then
   /bin/echo "Website appears online - today's date matches." >> $RUN_DIR/$DROP_MSG_FILE
 /bin/echo "Here are today's colors:" >> $RUN_DIR/$DROP_MSG_FILE
         /bin/echo "" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "The website is possibly down. Call $QUALIIER_1." >> $RUN_DIR/$DROP_MSG_FILE
	/bin/echo "Today's date does not match the site, has it been updated?" >> $RUN_DIR/$DROP_MSG_FILE
	/bin/echo "The site's date is listed as $SITE_DATE." >> $RUN_DIR/$DROP_MSG_FILE
fi

chmod -R 755 $RUN_DIR

echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.innerText15_ContentDiv"| lynx -stdin -dump | awk '{$1=$1};1' | sed '/^$/d' >> $RUN_DIR/$DROP_MSG_FILE

/bin/cat $RUN_DIR/$DROP_MSG_FILE
