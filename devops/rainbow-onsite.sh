#!/bin/bash

TODAYS_DATE=$(date +'%m/%d/%Y')
YEAR=`date +'%Y'`
DROP_MSG_FILE=onsite-dropmessage.txt
RUN_DIR=/options/officer-rainbow #this directory better not exist!
GET_URL=http://onsitesubstanceabusetesting.com #no slash at the end please
GET_URI_REAL=Daily-Testing-Colors.html
GET_URI=onsite.html
SITE_DATE=`echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.innerText14_ContentDiv"| lynx -stdin -dump | grep $YEAR`
GET_HOST=`echo $GET_URL | sed 's/http:\/\///g'`
QUALIFIER_1=248.242.0798

echo "Site Date = $SITE_DATE"
sleep 1
echo "System Date = $TODAYS_DATE"
echo $DROP_MSG_FILE
sleep 1
echo $RUN_DIR
sleep 1
echo $GET_URL
sleep 1
echo $GET_URI
sleep 1
echo $QUALIFIER_1
sleep 1
echo $GET_HOST

mkdir -p $RUN_DIR

/bin/rm -rf $RUN_DIR/$GET_URI*
/bin/rm -rf $RUN_DIR/$DROP_MSG_FILE
/bin/touch $RUN_DIR/$DROP_MSG_FILE

echo $TODAYS_DATE
sleep 1

echo "This file is named $RUN_DIR/$DROP_MSG_FILE" >> $RUN_DIR/$DROP_MSG_FILE

if [ `nc -w5 -z $GET_HOST 80 ; echo $?` = 1 ]; then
    	echo "The site appears to be OFFLINE" >&2
	echo "The site appears to be OFFLINE - the http connection failed" >> $RUN_DIR/$DROP_MSG_FILE
		sleep 5
		exit 1
	else echo "The site appears to be ONLINE"
		echo "The site appears to be ONLINE - the http connection succeeded" >> $RUN_DIR/$DROP_MSG_FILE
		sleep 5
fi

/usr/bin/wget -O $RUN_DIR/$GET_URI $GET_URL/$GET_URI_REAL
/bin/echo "Last run date is `date`" >>  $RUN_DIR/$DROP_MSG_FILE

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep $QUALIFIER_1 ; then
   /bin/echo "Website appears online - conducting secondary test." >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "The website is possibly down. Call 248-242-0798." >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep $TODAYS_DATE ; then
   /bin/echo "Website appears online - today's date matches." >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "The website is possibly down. Call 248.242.0798." >> $RUN_DIR/$DROP_MSG_FILE
	/bin/echo "Today's date does not match the site, has it been updated?" >> $RUN_DIR/$DROP_MSG_FILE
	/bin/echo "The site's date is listed as $SITE_DATE." >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Yellow ; then
   /bin/echo "Yellow WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Yellow was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Blue ; then
   /bin/echo "Blue WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Blue was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Purple ; then
   /bin/echo "Purple WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Purple was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Green ; then
   /bin/echo "Green WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Green was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Fuschia ; then
   /bin/echo "Fuschia WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Fuschia was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Gray ; then
   /bin/echo "Gray WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Gray was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Beige ; then
   /bin/echo "Beige WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Beige was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Ivory ; then
   /bin/echo "Ivory WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Ivory was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Magenta ; then
   /bin/echo "Magenta WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Magenta was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Peach ; then
   /bin/echo "Peach WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Peach was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Red ; then
   /bin/echo "Red WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Red was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Brown ; then
   /bin/echo "Brown WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Brown was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Gold ; then
   /bin/echo "Gold WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Gold was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Lavender ; then
   /bin/echo "Lavender WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Lavender was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Teal ; then
   /bin/echo "Teal WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Teal was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Amber ; then
   /bin/echo "Amber WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Amber was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Pink ; then
   /bin/echo "Pink WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Pink was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep White ; then
   /bin/echo "White WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "White was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Coral ; then
   /bin/echo "Coral WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Coral was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Orange ; then
   /bin/echo "Orange WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Orange was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep Black ; then
   /bin/echo "Black WAS chosen!" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "Black was NOT chosen!" >> $RUN_DIR/$DROP_MSG_FILE
fi

chmod -R 755 $RUN_DIR

#for JAMS = cat index.html | grep -e '<td>'

echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.innerText15_ContentDiv"| lynx -stdin -dump >> $RUN_DIR/$DROP_MSG_FILE

/bin/cat $RUN_DIR/$DROP_MSG_FILE
