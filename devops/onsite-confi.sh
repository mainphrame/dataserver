#!/bin/bash
#while read name; do
#  grep "$name" /options/officer-rainbow/onsite-ibm-clean.txt
#done </options/officer-rainbow/onsite-colors-raw.txt >/options/officer-rainbow/found.txt

while read color;
do
	grep "$color" /options/officer-rainbow/onsite-ibm-clean.txt | tr ' ' '\n' | sed '/^$/d' | grep "$color"
	
done < /options/officer-rainbow/onsite-colors-raw.txt > /options/officer-rainbow/onsite-confidence-final.txt

while read options;
do
	grep "$options" /options/officer-rainbow/onsite-confidence-raw.txt
done < /options/officer-rainbow/onsite-options.txt > /options/officer-rainbow/onsite-options-found.txt

item1=`cat /options/officer-rainbow/onsite-confidence-final.txt | wc -l` 
item2=`cat /options/officer-rainbow/onsite-options-found.txt | wc -l`
item3=`echo $((item1 + item2))`
total1=`cat /options/officer-rainbow/onsite-colors-raw.txt | wc -l`
total2=`cat /options/officer-rainbow/onsite-options.txt | wc -l`
total3=`echo $((total1 + total2))`
echo "$item3*100/$total3" | bc > /options/officer-rainbow/onsite-confidence.txt
/bin/rm -rf /options/officer-rainbow/onsite-colors-raw.txt
/bin/rm -rf /options/officer-rainbow/onsite-confidence-final.txt
/bin/rm -rf /options/officer-rainbow/onsite-options-found.txt
/bin/rm -rf /options/officer-rainbow/onsite-confidence-raw.txt

