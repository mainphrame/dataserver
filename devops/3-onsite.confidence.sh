#!/bin/bash

TODAYS_DATE=$(date +'%m/%d/%Y')
YEAR=`date +'%Y'`
HTML_MSG_FILE=onsite-dropmessage.html
DROP_MSG_FILE=onsite-dropmessage.html
HTML_ONSITE_FILE=onsite-colors.html
RAW_COLORS=onsite-colors-raw.txt
JUST_COLORS=onsite-colors.txt
CONFIDENCE_FILE=onsite-confidence-raw.txt
FINAL_CONFIDENCE_FILE=onsite-confidence.txt
RUN_DIR=/options/officer-rainbow #this directory better not exist!
GET_URL=http://onsitesubstanceabusetesting.com #no slash at the end please
GET_URI_REAL=Daily-Testing-Colors.html
GET_URI=onsite.html 
SITE_DATE=`echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.innerText14_ContentDiv"| lynx -stdin -dump | grep $YEAR`
GET_HOST=`echo $GET_URL | sed 's/http:\/\///g'`
QUALIFIER_1=248.242.0798

echo "Site Date = $SITE_DATE"
sleep 1
echo "System Date = $TODAYS_DATE"
echo $DROP_MSG_FILE
sleep 1
echo $RUN_DIR
sleep 1
echo $GET_URL
sleep 1
echo $GET_URI
sleep 1
echo $QUALIFIER_1
sleep 1
echo $GET_HOST

mkdir -p $RUN_DIR

/bin/rm -rf $RUN_DIR/$GET_URI*
/bin/rm -rf $RUN_DIR/$DROP_MSG_FILE
/bin/rm -rf $RUN_DIR/$HTML_MSG_FILE
/bin/rm -rf $RUN_DIR/$HTML_ONSITE_FILE
/bin/rm -rf $RUN_DIR/$RAW_COLORS
/bin/rm -rf $RUN_DIR/$JUST_COLORS
/bin/rm -rf $RUN_DIR/$CONFIDENCE_FILE
/bin/rm -rf $RUN_DIR/$FINAL_CONFIDENCE_FILE
/bin/touch $RUN_DIR/$DROP_MSG_FILE
/bin/touch $RUN_DIR/$HTML_MSG_FILE
/bin/touch $RUN_DIR/$HTML_ONSITE_FILE

echo $TODAYS_DATE
sleep 1

echo "<html>
<head>
<title>" >> $RUN_DIR/$HTML_MSG_FILE

echo "This file is named $RUN_DIR/$DROP_MSG_FILE" >> $RUN_DIR/$DROP_MSG_FILE

if [ `nc -w5 -z $GET_HOST 80 ; echo $?` = 1 ]; then
    		echo "The site appears to be OFFLINE" >&2
		echo "The site appears to be OFFLINE - the http connection failed" >> $RUN_DIR/$DROP_MSG_FILE
		echo "OFFLINE" >> $RUN_DIR/$CONFIDENCE_FILE
		sleep 5
		exit 1
	else 	echo "The site appears to be ONLINE"
		echo "The site appears to be ONLINE - the http connection succeeded" >> $RUN_DIR/$DROP_MSG_FILE
		echo "ONLINE" >> $RUN_DIR/$CONFIDENCE_FILE
		
		sleep 5
fi

/usr/bin/wget -O $RUN_DIR/$GET_URI $GET_URL/$GET_URI_REAL
/bin/echo "Last run date is `date`" >>  $RUN_DIR/$DROP_MSG_FILE

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep $QUALIFIER_1 ; then
   		/bin/echo "Website appears online - conducting secondary test." >> $RUN_DIR/$DROP_MSG_FILE
   		/bin/echo "QUALIFIER MATCH" >> $RUN_DIR/$CONFIDENCE_FILE
else
   		/bin/echo "The website is possibly down. Call $QUALIFIER_1." >> $RUN_DIR/$DROP_MSG_FILE
   		/bin/echo "QUALIFIER MISMATCH" >> $RUN_DIR/$CONFIDENCE_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep $TODAYS_DATE ; then
   		/bin/echo "Website appears online - today's date matches." >> $RUN_DIR/$DROP_MSG_FILE
 		/bin/echo "Here are today's colors:" >> $RUN_DIR/$DROP_MSG_FILE
         	/bin/echo "" >> $RUN_DIR/$DROP_MSG_FILE
		/bin/echo "DATE MATCH" >> $RUN_DIR/$CONFIDENCE_FILE
else
   /bin/echo "The website is possibly down. Call $QUALIIER_1." >> $RUN_DIR/$DROP_MSG_FILE
		/bin/echo "Today's date does not match the site, has it been updated?" >> $RUN_DIR/$DROP_MSG_FILE
		/bin/echo "The site's date is listed as $SITE_DATE." >> $RUN_DIR/$DROP_MSG_FILE
		/bin/echo "DATE MISMATCH" >> $RUN_DIR/$CONFIDENCE_FILE
fi

chmod -R 755 $RUN_DIR

echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.innerText15_ContentDiv"| lynx -stdin -dump | awk '{$1=$1};1' | sed '/^$/d' >> $RUN_DIR/$HTML_MSG_FILE

echo "</title>
</head>
</html>" >> $RUN_DIR/$HTML_MSG_FILE

echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.innerText15_ContentDiv"| lynx -stdin -dump | awk '{$1=$1};1' | sed '/^$/d' | tr '[:upper:]' '[:lower:]' > $RUN_DIR/$JUST_COLORS
echo "<----->" >> $RUN_DIR/$JUST_COLORS

echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.innerText15_ContentDiv"| lynx -stdin -dump | awk '{$1=$1};1' | sed '/^$/d' | tr '[:upper:]' '[:lower:]' > $RUN_DIR/$RAW_COLORS

echo "<html>
<head>
<title>" >> $RUN_DIR/$HTML_ONSITE_FILE

echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.innerText15_ContentDiv"| lynx -stdin -dump | awk '{$1=$1};1' | sed '/^$/d' | tr '[:upper:]' '[:lower:]' >> $RUN_DIR/$HTML_ONSITE_FILE

echo "</title>
</head>
</html>" >> $RUN_DIR/$HTML_ONSITE_FILE

/bin/cat $RUN_DIR/$JUST_COLORS
/bin/cat $RUN_DIR/$DROP_MSG_FILE
/bin/cat $RUN_DIR/$HTML_MSG_FILE
/bin/cat $RUN_DIR/$CONFIDENCE_FILE

while read color;
do
        grep "$color" /options/officer-rainbow/onsite-ibm-clean.txt | tr ' ' '\n' | sed '/^$/d' | grep "$color"

done < $RUN_DIR/$RAW_COLORS > /options/officer-rainbow/onsite-confidence-final.txt

while read options;
do
        grep "$options" $RUN_DIR/$CONFIDENCE_FILE
done < /options/officer-rainbow/onsite-options.txt > /options/officer-rainbow/onsite-options-found.txt

item1=`cat /options/officer-rainbow/onsite-confidence-final.txt | wc -l`
item2=`cat /options/officer-rainbow/onsite-options-found.txt | wc -l`
item3=`echo $((item1 + item2))`
total1=`cat /options/officer-rainbow/onsite-colors-raw.txt | wc -l`
total2=`cat /options/officer-rainbow/onsite-options.txt | wc -l`
total3=`echo $((total1 + total2))`
echo "$item3*100/$total3" | bc > /options/officer-rainbow/onsite-confidence.txt
/bin/rm -rf /options/officer-rainbow/onsite-colors-raw.txt
/bin/rm -rf /options/officer-rainbow/onsite-confidence-final.txt
/bin/rm -rf /options/officer-rainbow/onsite-options-found.txt
/bin/rm -rf /options/officer-rainbow/onsite-confidence-raw.txt
