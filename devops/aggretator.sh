#!/bin/bash

rm -rf /options/officer-rainbow/aggregate.html
rm -rf /options/officer-rainbow/aggregate.txt

echo "<html>
<head>
<title>" >> /options/officer-rainbow/aggregate.html

cat /options/officer-rainbow/*-dropmessage.html | grep -v -e "<html>" -e "<head>" -e "<title>" -e "</html>" -e "</head>" -e "</title>" >> /options/officer-rainbow/aggregate.txt
cat /options/officer-rainbow/aggregate.txt >> /options/officer-rainbow/aggregate.html

echo "</title>
</head>
</html>" >> /options/officer-rainbow/aggregate.html
