#!/bin/bash

TODAYS_DATE=`date +'%B %d, %Y'`
YEAR=`date +'%Y'`
DROP_MSG_FILE=nova-dropmessage.txt
RUN_DIR=/options/officer-rainbow #this directory better not exist!
GET_URL=https://www.nova-testing.com #no slash at the end please
GET_URI=nova.html
GET_URI_REAL=""
SITE_DATE=`echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.mod_required_tests"| lynx -stdin -dump | grep $YEAR`
GET_HOST=`echo $GET_URL | sed 's/http:\/\///g' | sed 's/https:\/\///g'`
QUALIFIER_1=248-788-7058

echo "Site Date = $SITE_DATE"
sleep 1
echo "System Date = $TODAYS_DATE"
echo $DROP_MSG_FILE
sleep 1
echo $RUN_DIR
sleep 1
echo $GET_URL
sleep 1
echo $GET_URI
sleep 1
echo $QUALIFIER_1

mkdir -p $RUN_DIR

/bin/rm -rf $RUN_DIR/$GET_URI*
/bin/rm -rf $RUN_DIR/$DROP_MSG_FILE
/bin/touch $RUN_DIR/$DROP_MSG_FILE

echo "This file is named $RUN_DIR/$DROP_MSG_FILE" >> $RUN_DIR/$DROP_MSG_FILE

if [ `nc -w5 -z $GET_HOST 80 ; echo $?` = 1 ]; then
        echo "The site appears to be OFFLINE" >&2
        echo "The site appears to be OFFLINE - the http connection failed" >> $RUN_DIR/$DROP_MSG_FILE
                sleep 5
                exit 1
        else echo "The site appears to be ONLINE"
                echo "The site appears to be ONLINE - the http connection succeeded" >> $RUN_DIR/$DROP_MSG_FILE
                sleep 5
fi

/usr/bin/wget -O $RUN_DIR/$GET_URI $GET_URL/$GET_URI_REAL
/bin/echo "Last run date is `date`" >>  $RUN_DIR/$DROP_MSG_FILE

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep $QUALIFIER_1 ; then
   /bin/echo "Website appears online - conducting secondary test." >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "The website is possibly down. Call $QUALIFIER_1." >> $RUN_DIR/$DROP_MSG_FILE
fi

if /bin/cat $RUN_DIR/$GET_URI | /bin/grep "$TODAYS_DATE" ; then
   /bin/echo "Website appears online - today's date matches." >> $RUN_DIR/$DROP_MSG_FILE
 /bin/echo "Here are today's colors:" >> $RUN_DIR/$DROP_MSG_FILE
         /bin/echo "" >> $RUN_DIR/$DROP_MSG_FILE
else
   /bin/echo "The website is possibly down. Call $QUALIFIER_1." >> $RUN_DIR/$DROP_MSG_FILE
fi

echo "$GET_URL/$GET_URI_REAL" | wget -O- -i- | hxnormalize -x | hxselect -i "div.mod_required_tests"| lynx -stdin -dump | grep -v $YEAR| grep -v Colors | tr , '\n' | awk '{$1=$1};1' >> $RUN_DIR/$DROP_MSG_FILE

/bin/cat $RUN_DIR/$DROP_MSG_FILE

