import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.instance.Sms;

import java.util.HashMap;
import java.util.Map;

import spark.Spark;

import static spark.Spark.get;
import static spark.Spark.post;

public class SMSBackend {
    public static void main(String[] args) {

        //Heroku assigns different port each time, hence reading it from process.
        ProcessBuilder process = new ProcessBuilder();
        Integer port;
        if (process.environment().get("PORT") != null) {
            port = Integer.parseInt(process.environment().get("PORT"));
        } else {
            port = 8080;
        }
        Spark.port(port);


        get("/", (req, res) -> "Officer Rainbow - GURU Meditation");

        String twilio_accound_sid = "AC7407fc54e4c5ec8cddebf1e7d9891f59";
        String twilio_auth_token = "6fb59e7cffd8c650697de8e94bbe4ffb";
        String twilio_number = "+12484793526";

        TwilioRestClient client = new TwilioRestClient(twilio_accound_sid, twilio_auth_token);

        post("/sms-text", (req, res) -> {
            String body = req.queryParams("Body");
            String to = req.queryParams("To");

            Map<String, String> callParams = new HashMap<>();
            callParams.put("To", to);
            callParams.put("From", twilio_number);
            callParams.put("Body", body);
            Sms message = client.getAccount().getSmsFactory().create(callParams);

            return message.getSid();
        });

    }
}
