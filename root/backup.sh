git config pack.windowMemory 10m
git config pack.packSizeLimit 20m
git config --global pack.windowMemory 256m
git config --global pack.threads 1
git add --all
git commit -am "daily backup"
git push -u data master
