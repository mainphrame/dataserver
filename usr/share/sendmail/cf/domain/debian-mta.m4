divert(-1)dnl
#-----------------------------------------------------------------------------
# $Sendmail: debian-mta.m4,v 8.15.2 2015-12-10 18:02:49 cowboy Exp $
#
# Copyright (c) 1998-2010 Richard Nelson.  All Rights Reserved.
#
# cf/domain/debian-mta.m4.  Generated from debian-mta.m4.in by configure.
#
# domain(debian-mta) config file for building Sendmail 8.15.2-3
#
# Note: the .in file supports 8.7.6 - 9.0.0, but the generated
#	file is customized to the version noted above.
#
#-----------------------------------------------------------------------------
#
divert(0)dnl
dnl #
dnl #---------------------------------------------------------------------
dnl # Bring in Autoconf results
dnl #---------------------------------------------------------------------
ifdef(`sm_version', `dnl' ,
`include(`/usr/share/sendmail/cf/debian/autoconf.m4')dnl')
dnl #
VERSIONID(`$Id: debian-mta.m4, v 8.15.2-3 2015-12-10 18:02:49 cowboy Exp $')
define(`DEBIAN_MTA')dnl
dnl #
dnl # changes made herein *must* be reflected in parse_mc,update_db,debian.m4
dnl #
define(`STATUS_FILE',			`/var/lib/sendmail/sendmail.st')dnl
define(`confHOST_STATUS_DIRECTORY',
	`/var/lib/sendmail/host_status')dnl
dnl #
dnl # For FHS, we use a subdirectory in /var/run (multiple files)
dnl # For permissions, we use separate MSP/MTA subdirectories
define(`confPID_FILE',		`/var/run/sendmail/mta/sendmail.pid')dnl
define(`confCONTROL_SOCKET_NAME',
	`/var/run/sendmail/mta/smcontrol')dnl
dnl #
dnl # flags
define(`confPROCESS_TITLE_PREFIX',	`MTA')dnl
define(`confMAX_DAEMON_CHILDREN',	`0')dnl
dnl define(`confNO_RCPT_ACTION',		`add-to-undisclosed')dnl
define(`confRRT_IMPLIES_DSN',		`False')dnl
define(`confSAFE_FILE_ENV',			`/')dnl
define(`confSAFE_QUEUE',			`True')dnl
define(`confQUEUE_FILE_MODE',		`0600')dnl
define(`confTEMP_FILE_MODE',        `0600')dnl
dnl #
dnl # Provide some 'more reasonable' timeout values
dnl # See SASL/TLS sections below for more timeout values
dnl #
define(`confTO_ICONNECT', `2m')dnl      # rfc min=  , def=5m
define(`confTO_MAIL',     `2m')dnl      # rfc min=5m, def=10m, too long
define(`confTO_DATAINIT', `2m')dnl      # rfc min=2m, def=5m
define(`confTO_RSET',     `1m')dnl      # rfc min=  , def=5m
define(`confTO_QUIT',     `2m')dnl      # rfc min=  , def=2m
define(`confTO_COMMAND',  `5m')dnl      # rfc min=5m, def=1h
define(`confTO_IDENT',    `5s')dnl      #           , def=5s, 0=skip
dnl #
dnl RFC 2821 recommends a higher value for max-hop than the default(25)
define(`confMAX_HOP', `100')dnl
dnl #
dnl # Debian users have group writable directories/files by default (8.9.0+)
ifelse(eval(sm_version_math >= 526592), `1',dnl
`define(`confDONT_BLAME_SENDMAIL',dnl
defn(`confDONT_BLAME_SENDMAIL')`,AssumeSafeChown,ForwardFileInGroupWritableDirPath,GroupWritableForwardFileSafe,GroupWritableIncludeFileSafe,IncludeFileInGroupWritableDirPath')dnl'
)dnl
dnl #
dnl # Don't warn for non-existant forward files (8.10.0+)
ifelse(eval(sm_version_math >= 526848), `1',dnl
`define(`confDONT_BLAME_SENDMAIL',dnl
defn(`confDONT_BLAME_SENDMAIL')`,DontWarnForwardFileInUnsafeDirPath,TrustStickyBit,NonRootSafeAddr')dnl'
)dnl
dnl #
dnl # Allow group-writable include files (8.12.0+)
ifelse(eval(sm_version_math >= 527360), `1',dnl
`define(`confDONT_BLAME_SENDMAIL',dnl
defn(`confDONT_BLAME_SENDMAIL')`,GroupWritableIncludeFile')dnl'
)dnl
dnl # typo in 8.12.0+, fixed in 8.15
ifelse(eval(sm_version_math >= 527360), `1',dnl
`ifelse(eval(sm_version_math < 528128), `1',dnl
`define(`confDONT_BLAME_SENDMAIL',dnl
defn(`confDONT_BLAME_SENDMAIL')`,GroupReadableaDefaultAuthInfoFile')',dnl
`define(`confDONT_BLAME_SENDMAIL',dnl
defn(`confDONT_BLAME_SENDMAIL')`,GroupReadableDefaultAuthInfoFile')'dnl
)')dnl
dnl #
dnl # by default, disable Message Submission Agent (8.10.0+)
dnl # except for 8.12.0+, where its needed for MSA !
ifelse(eval(sm_version_math >= 526848), `1',dnl
`ifelse(eval(sm_version_math < 527360), `1',dnl
`FEATURE(`no_default_msa')',`dnl')')
dnl #
dnl # Allow mailq/hoststat to work with both MSP/MTA queues
ifelse(eval(sm_version_math >= 527360), `1',dnl
`define(`confQUEUE_FILE_MODE', `0640')dnl'
`define(`confTEMP_FILE_MODE', `0640')dnl')
dnl #
dnl # For security, we default to not letting lusers run the queues
dnl # If possible, we prevent sendmail -bv from reading things they shouldn't
dnl #
define(`confPRIVACY_FLAGS', `restrictqrun')
ifelse(eval(sm_version_math >= 527360), `1',dnl
`define(`confPRIVACY_FLAGS',dnl
    defn(`confPRIVACY_FLAGS')`,restrictexpand')dnl')
dnl #
dnl # Define trusted user to be the new mta uid (smmta)
dnl #
ifdef(`confTRUSTED_USER',,`define(`confTRUSTED_USER',`smmta')dnl')
dnl #
dnl # Provide a more reasonable default for number of queue-runners
dnl # because many/most? people will not discover this themselves
dnl # and the sendmail default is one!
dnl #
define(`confMAX_RUNNERS_PER_QUEUE',     `5')dnl
dnl #
dnl # Optional items (should be a subset site.config.m4 used for build)
dnl # to prevent sendmail error messages
dnl #
