divert(-1)dnl
#-----------------------------------------------------------------------------
# $Sendmail: debian-msp.m4,v 8.15.2 2015-12-10 18:02:49 cowboy Exp $
#
# Copyright (c) 1998-2010 Richard Nelson.  All Rights Reserved.
#
# cf/domain/debian-msp.m4.  Generated from debian-msp.m4.in by configure.
#
# domain(debian-msp) config file for building Sendmail 8.15.2-3
#
# Note: the .in file supports 8.7.6 - 9.0.0, but the generated
#	file is customized to the version noted above.
#
#-----------------------------------------------------------------------------
#
divert(0)dnl
dnl #
dnl #---------------------------------------------------------------------
dnl # Bring in Autoconf results
dnl #---------------------------------------------------------------------
ifdef(`sm_version', `dnl',
`include(`/usr/share/sendmail/cf/debian/autoconf.m4')dnl')
dnl #
VERSIONID(`$Id: debian-msp.m4, v 8.15.2-3 2015-12-10 18:02:49 cowboy Exp $')
define(`DEBIAN_MSP')dnl
define(`confCF_VERSION', `Submit')dnl
dnl #
dnl # changes made herein *must* be reflected in parse_mc,update_db,debian.m4
dnl #
define(`MSP_QUEUE_DIR',			`/var/spool/mqueue-client')dnl
define(`STATUS_FILE',			`/var/lib/sendmail/sm-client.st')dnl
dnl #
dnl # Since MSP only delivers to MTA, persistant status isn't needed
define(`confHOST_STATUS_DIRECTORY', `')dnl
dnl #
dnl # For FHS, we use a subdirectory in /var/run (multiple files)
dnl # For permissions, we use separate MSP/MTA subdirectories
define(`confPID_FILE',		`/var/run/sendmail/msp/sendmail.pid')dnl
define(`confCONTROL_SOCKET_NAME',
	`/var/run/sendmail/msp/smcontrol')dnl
dnl #
dnl # flags
define(`confPROCESS_TITLE_PREFIX',	`MSP')dnl
dnl define(`confNO_RCPT_ACTION',		`add-to-undisclosed')dnl
define(`confRRT_IMPLIES_DSN',		`False')dnl
define(`confSAFE_FILE_ENV',			`/')dnl
define(`confSAFE_QUEUE',			`True')dnl
define(`confQUEUE_FILE_MODE',		`0660')dnl
define(`confTEMP_FILE_MODE',		`0640')dnl
dnl #
dnl # Provide some 'more reasonable' timeout values
dnl # See SASL/TLS sections below for more timeout values
dnl #
define(`confTO_ICONNECT', `2s')dnl      # rfc min=  , def=5m
define(`confTO_MAIL',     `2m')dnl      # rfc min=5m, def=10m, too long
define(`confTO_DATAINIT', `2m')dnl      # rfc min=2m, def=5m
define(`confTO_RSET',     `1m')dnl      # rfc min=  , def=5m
define(`confTO_QUIT',     `2m')dnl      # rfc min=  , def=2m
define(`confTO_COMMAND',  `5m')dnl      # rfc min=5m, def=1h
define(`confTO_IDENT',    `5s')dnl      #           , def=5s, 0=skip
define(`confTO_HOSTSTATUS',`0')dnl      #           , def=30m
dnl #
dnl # by default, disable Message Submission Agent (8.10.0+)
ifelse(eval(sm_version_math >= 526848), `1',dnl
`FEATURE(`no_default_msa')')
dnl #
dnl # Mail Submission Program uid/gid
define(`confRUN_AS_USER', `smmsp')dnl
define(`confTRUSTED_USER', confRUN_AS_USER)dnl
dnl #
dnl # Optional items (should be a subset site.config.m4 used for build)
dnl # to prevent sendmail error messages
dnl #
