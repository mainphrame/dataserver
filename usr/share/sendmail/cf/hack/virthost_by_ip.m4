divert(-1)dnl
#-----------------------------------------------------------------------------
# $Sendmail: virthost_by_ip.m4,v 8.15.2 2015-12-10 18:02:49 cowboy Exp $
#
# Copyright (c) 2001-2010 Richard Nelson.  All Rights Reserved.
#
# cf/hack/virthost_by_ip.m4.  Generated from virthost_by_ip.m4.in by configure.
#
# hack(virthost_by_ip) config file for building Sendmail 8.15.2-3
# Note: Also check op.{ps,txt} for the 'b' modifier to daemon_options - it
#	extends ip based virtual hosting.
#
# Note: the .in file supports 8.7.6 - 9.0.0, but the generated
#	file is customized to the version noted above.
#
#-----------------------------------------------------------------------------
#
divert(0)dnl
dnl #
dnl #---------------------------------------------------------------------
dnl # Bring in Autoconf results
dnl #---------------------------------------------------------------------
ifdef(`sm_version', `dnl',
`include(`/usr/share/sendmail/cf/debian/autoconf.m4')dnl')
dnl #
VERSIONID(`$Id: virthost_by_ip.m4, v 8.15.2-3 2015-12-10 18:02:49 cowboy Exp $')
dnl #
dnl #---------------------------------------------------------------------
dnl # Virtual hosting extensions - Login (greeting message)
dnl #---------------------------------------------------------------------
define(`confSMTP_LOGIN_MSG', `$?{if_name}${if_name}$|$j$. Sendmail $v/$Z; $b; (No UCE/UBE) $?{client_addr}logging access from: ${client_name}(${client_resolve})-$_$.')dnl
dnl #
dnl #---------------------------------------------------------------------
dnl # Virtual hosting extensions - Received-by headers (8.12.0 style)
dnl #---------------------------------------------------------------------
dnl # 8.7.0+
ifdef(`_REC_HDR_', `dnl',
`define(`_REC_HDR_', `$?sfrom $s $.$?_($?s$|from $.$_)')dnl'
`define(`_REC_END_', `for $u; $|;
	$.$b')dnl'
)
dnl # AUTH(SASL) 8.10.0+
ifdef(`_REC_AUTH_', `dnl',
`define(`_REC_AUTH_', `$.$?{auth_type}(authenticated')dnl'
`define(`_REC_FULL_AUTH_', `$.$?{auth_type}(user=${auth_authen} $?{auth_author}author=${auth_author} $.mech=${auth_type}')dnl'
)
dnl # TLS(SSL) 8.11.0+
ifdef(`_REC_BY_', `dnl',
`define(`_REC_BY_', `$.by $j ($v/$Z)$?r with $r$. id $i$?{tls_version}')dnl'
`define(`_REC_TLS_', `(version=${tls_version} cipher=${cipher} bits=${cipher_bits} verify=${verify})$.$?u')dnl'
)
dnl # Now, override default settings for virtual hosting
define(`_REC_BY_', `$.by $?{if_name}${if_name}$|$j$. ($v/$Z)$?r with $r$. id $i$?{tls_version}')dnl
define(`confRECEIVED_HEADER', `_REC_HDR_
	_REC_FULL_AUTH_$?{auth_ssf} bits=${auth_ssf}$.)
	_REC_BY_
	_REC_TLS_
	_REC_END_')dnl
dnl #
dnl #---------------------------------------------------------------------
dnl # Virtual hosting extensions - ClientPortOptions (modifier=h)
dnl # -- Use name of interface for HELO command
dnl #---------------------------------------------------------------------
ifelse(eval(sm_version_math >= 526848), `1',dnl
`ifelse(defn(`_CPO_'), `dnl',
`CLIENT_OPTIONS(`Modifier=h')dnl')dnl')
dnl #
dnl #---------------------------------------------------------------------
dnl # Virtual hosting extensions - DaemonPortOptions (modifier=b)
dnl # -- Bind to interface mail was received on
dnl # I can't do this for you, because each use of DAEMON_OPTIONS will
dnl # create a new listener !
dnl #---------------------------------------------------------------------
dnl ifelse(eval(sm_version_math >= 526848), `1',dnl
dnl `ifelse(defn(`_DPO_'), `dnl',
dnl `DAEMON_OPTIONS(`Modifier=b')dnl')dnl')

