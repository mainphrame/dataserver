divert(-1)dnl
#-----------------------------------------------------------------------------
# $Sendmail: autoconf.m4,v 8.15.2 2015-12-10 18:02:49 cowboy Exp $
#
# Copyright (c) 2001-2010 Richard Nelson.  All Rights Reserved.
#
# cf/debian/autoconf.m4.  Generated from autoconf.m4.in by configure.
#
# m4 autoconf config file for building Sendmail 8.15.2-3
#
# Note: the .in file supports 8.7.6 - 9.0.0, but the generated
#	file is customized to the version noted above.
#
#-----------------------------------------------------------------------------
#
divert(0)dnl
VERSIONID(`$Id: autoconf.m4, v 8.15.2-3 2015-12-10 18:02:49 cowboy Exp $')
dnl #
dnl #---------------------------------------------------------------------
dnl # Export (from autoconf to m4) some items of dubious value
dnl #---------------------------------------------------------------------
define(`SM_VERS',			`8.14.4')dnl
define(`SM_DATE',			`2010-10-11 17:53:00')dnl
define(`SM_MINVERS',		`8.7.6')dnl
define(`SM_MAXVERS',		`9.0.0')dnl
define(`SM_CPYRT',			`2010')dnl
define(`sm_date',			`2015-12-10')dnl
define(`sm_time',			`18:02:49')dnl
define(`sm_utc',			`+0100')dnl
define(`sm_version',		`8.15.2')dnl
define(`sm_revision',		`-3')dnl
define(`sm_version_v',		`8')dnl
define(`sm_version_r',		`15')dnl
define(`sm_version_major',	`8.15')dnl
define(`sm_version_minor',	`2')dnl
define(`sm_version_beta',	`')dnl
define(`sm_version_math',	`528130')dnl
define(`sm_enable_regex',	`yes')dnl
define(`sm_enable_ndbm',	`no')dnl
define(`sm_enable_newdb',	`yes')dnl
define(`sm_newdb_lib',		`-ldb')dnl
define(`sm_enable_nis',		`yes')dnl
define(`sm_enable_nisplus',	`yes')dnl
define(`sm_enable_ldap',	`yes')dnl
define(`sm_ldap_lib',		`-lldap -llber')dnl
define(`sm_enable_hesiod',	`no')dnl
define(`sm_hesiod_parms',	`')dnl
define(`sm_enable_tcpd',	`yes')dnl
define(`sm_enable_ipv6',	`yes')dnl
define(`sm_enable_maillock',`yes')dnl
define(`sm_enable_milter',	`yes')dnl
define(`sm_enable_sfio',	`no')dnl
define(`sm_enable_auth',	`yes')dnl
define(`sm_auth_lib',		`2')dnl
define(`sm_enable_tls',		`yes')dnl
define(`sm_enable_shm',		`yes')dnl
define(`sm_ffr',			` -D_FFR_QUEUE_SCHED_DBG -D_FFR_RESET_MACRO_GLOBALS -D_FFR_TLS_1 -D_FFR_TLS_EC -D_FFR_DEAL_WITH_ERROR_SSL')dnl
define(`sm_m4_ffr',			`')dnl
define(`sm_enable_dev',		`@sm_enable_dev@')dnl
define(`sm_enable_doc',		`@sm_enable_doc@')dnl
