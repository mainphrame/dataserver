server {
        listen 80;
        listen [::]:80;

        root /options/officer-rainbow/portal;
	index index.php;

        # Make site accessible from http://localhost/
        server_name tescron.com portal.tescron.com;

        location / {
               autoindex on;
        }

        location ~ \.php$ {
                include snippets/fastcgi-php.conf;

 # With php7.0-cgi alone:
 # fastcgi_pass 127.0.0.1:9000;
 # With php7.0-fpm:
                fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        }
}
